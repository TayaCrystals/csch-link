import { IncomingMessage, ServerResponse } from 'http'
import { send } from 'micro'
const { parse } = require("url")
const UrlModel = require('./lib/urlModel')
const connectdb = require('./lib/db')
const role = require('./lib/role')

module.exports = async (req: IncomingMessage, res: ServerResponse) => {


  //
  // ─── SETUP ──────────────────────────────────────────────────────────────────────
  //

  // Require Role
  if (await role(req) !== 'admin') {
    send(res, 401, { Error: { code: 5, message: `Not Autorized` } })
    return
  }
  // Database Connection
  await connectdb()

  //
  // ─── INPUT PARSING ──────────────────────────────────────────────────────────────
  //

  const { query } = parse(req.url, true)
  const { alias } = query

  //
  // ─── REMOVE ──────────────────────────────────────────────────────────────
  //

  UrlModel.deleteOne({ alias: alias }, function (err, obj) {
    if (err) console.error(err)
    //TODO: check if not found
    send(res, 200, { message: `Deleted entry with alias: '${alias}'` })
  })

}
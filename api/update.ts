import { IncomingMessage, ServerResponse } from 'http'
import { send } from 'micro'
const { parse } = require("url")
const UrlModel = require('./lib/urlModel')
const connectdb = require('./lib/db')
const role = require('./lib/role')

module.exports = async (req: IncomingMessage, res: ServerResponse) => {

  //
  // ─── SETUP ──────────────────────────────────────────────────────────────────────
  //

  // Require Role
  if (await role(req) !== 'admin') {
    send(res, 401, { Error: { code: 5, message: `Not Autorized` } })
    return
  }

  // Database Connection
  await connectdb()

  //
  // ─── INPUT PARSING ──────────────────────────────────────────────────────────────
  //

  const { query } = parse(req.url, true)

  const { oldalias } = query
  if (oldalias === undefined) {
    send(res, 400, { message: `Please specify a alias to update` })
    return
  }
  let mongoobj = await UrlModel.findOne({ alias: oldalias })

  // ─── URL ─────────────────────────────────────────────────

  let { url } = query
  if (url === undefined) {
    url = mongoobj.url
  } else {
    if (!url.match(/https{0,1}:\/\/.*\..*/)) {
      url = 'http://' + url
    }
  }

  // ─── ALIAS ───────────────────────────────────────────────────────

  let alias = query.alias
  // If no alias is defined then generate a random one
  if (alias === undefined) {
    alias = mongoobj.alias
  } else {
    // For custom alias check if it is already in use
    if (await dbExists(alias)) {
      // If it is, throw error and return
      send(res, 409, { Error: { code: 2, message: `Alias already in use` } })
      return
    }

    if (!alias.match(/^[a-z0-9_-]*$/)) {
      // If custom alias includes not friendly characters, throw error and return
      send(res, 400, { Error: { code: 3, message: `Alias not only using lowercase characters, numbers, - and _` } })
      return
    }
  }


  //
  // ─── SAVING ─────────────────────────────────────────────────────────────────────
  //

  mongoobj.alias = alias
  mongoobj.url = url

  mongoobj.save()
    .then(() => {
      // Respond to the user that it worked
      send(res, 200, { alias: alias, url: url })
    })
    .catch(err => {
      console.error(err)
      send(res, 500, { Error: { code: 6, message: `Server Error` } })
    })
}

async function dbExists(alias): Promise<boolean> {
  const x = await UrlModel.find({ alias: alias })
  return x.length !== 0
}

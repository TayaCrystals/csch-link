import { IncomingMessage, ServerResponse } from 'http'
import { send } from 'micro'
const UrlModel = require('./lib/urlModel')
const connectdb = require('./lib/db')
const rateLimit = require('./lib/rateLimit')

module.exports = async (req: IncomingMessage, res: ServerResponse) => {

  //
  // ─── SETUP ──────────────────────────────────────────────────────────────────────
  //

  // Rate Limit
  const limit = await rateLimit(req, 1000, 1)
  if (!limit) {
    send(res, 429, { Error: { code: 7, message: `Too many requests`, value: limit } })
    return
  }

  // Database Connection
  await connectdb()

  //
  // ─── FIND ──────────────────────────────────────────────────────────────
  //

  UrlModel.find((err, obj) => {
    if (err) console.error(err)
    if (!obj) {
      send(res, 404, { message: 'nothing found' })
    } else {

      obj = obj.map(x => {
        return { alias: x.alias, url: x.url }
      })

      send(res, 200, obj)
    }
  })

}
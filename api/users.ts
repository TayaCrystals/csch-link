import { IncomingMessage, ServerResponse } from 'http'
import { send } from 'micro'
const UserModel = require('./lib/userModel')
const connectdb = require('./lib/db')
const role = require('./lib/role')

module.exports = async (req: IncomingMessage, res: ServerResponse) => {

  //
  // ─── SETUP ──────────────────────────────────────────────────────────────────────
  //

  // Require Role
  if (await role(req) !== 'admin') {
    send(res, 401, { Error: { code: 5, message: `Not Autorized` } })
    return
  }
  // Database Connection
  await connectdb()

  //
  // ─── FIND ──────────────────────────────────────────────────────────────
  //

  UserModel.find(function (err, obj) {
    if (err) console.error(err)
    send(res, 200, obj.map(x => { return { username: x.username, role: x.role } }))
  })

}

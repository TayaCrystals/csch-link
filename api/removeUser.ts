import { IncomingMessage, ServerResponse } from 'http'
import { send } from 'micro'
const { parse } = require("url")
const UserModel = require('./lib/userModel')
const connectdb = require('./lib/db')
const role = require('./lib/role')

module.exports = async (req: IncomingMessage, res: ServerResponse) => {


  //
  // ─── SETUP ──────────────────────────────────────────────────────────────────────
  //

  // Require Role
  if (await role(req) !== 'admin') {
    send(res, 401, { Error: { code: 5, message: `Not Autorized` } })
    return
  }
  // Database Connection
  await connectdb()

  //
  // ─── INPUT PARSING ──────────────────────────────────────────────────────────────
  //

  const { query } = parse(req.url, true)
  const { username } = query

  //
  // ─── REMOVE ──────────────────────────────────────────────────────────────
  //

  UserModel.deleteOne({ username: username }, function (err, obj) {
    if (err) console.error(err)
    send(res, 200, { message: `Deleted user with usernamname: '${username}'` })
  })

}

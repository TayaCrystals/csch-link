import mongoose from 'mongoose'

mongoose.Promise = global.Promise
let isConnected: number

module.exports = async () => {
  if (isConnected) {
    return Promise.resolve()
  }
  const db = await mongoose.connect(`mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}${process.env.MONGO_URL}`, { useNewUrlParser: true })
  isConnected = db.connections[0].readyState
  return db
}
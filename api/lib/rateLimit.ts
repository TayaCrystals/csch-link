import { IncomingMessage } from 'http'
const connectdb = require('./db')
const RequestModel = require('./requestModel')

module.exports = async (req: IncomingMessage, timeout: number, toleratedTries: number = 1) => {
    // Database Connection
    await connectdb()
    await RequestModel.deleteMany({ lastRequest: { $lt: Date.now() - timeout } }, (err, obj) => {
        console.log('removed old entries', obj)
    })

    const ip = (req.headers['x-forwarded-for'] || '').split(',').pop() || 
         req.connection.remoteAddress || 
         req.socket.remoteAddress || 
         req.connection.socket.remoteAddress

    const res = await RequestModel.findOne({ ip: ip })
    if (res) {
        console.log('found recorded ip')
        res.lastRequest = Date.now()

        if(res.tries < toleratedTries) {
            res.tries++
            res.save()
            return true
        } else {
            res.save()
            return false
        }
    } else {
        console.log('adding new ip')
        new RequestModel({ ip: ip, lastRequest: Date.now(), tries: 1 }).save()
            .then(() => {
                // Respond to the user that it worked
                console.log('added request to db')
            })
            .catch(err => {
                console.error(err)
            })
        return true
    }
}
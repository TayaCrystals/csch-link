import * as jwt from 'jsonwebtoken'

module.exports = (user) => {
  const data = {
    _id: user._id,
    username: user.username,
    role: user.role
  };
  const signature = process.env.JWT_SECRET;
  const expiration = '6h';

  return jwt.sign(data, signature, { expiresIn: expiration });
}

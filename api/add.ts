import { IncomingMessage, ServerResponse } from 'http'
import { send } from 'micro'
const { parse } = require("url")
const UrlModel = require('./lib/urlModel')
const connectdb = require('./lib/db')
const role = require('./lib/role')

module.exports = async (req: IncomingMessage, res: ServerResponse) => {

  //
  // ─── SETUP ──────────────────────────────────────────────────────────────────────
  //

  // Require Role
  if (await role(req) === undefined) {
    send(res, 401, { Error: { code: 5, message: `Not Authorized` } })
    return
  }

  // Database Connection
  await connectdb()

  //
  // ─── INPUT PARSING ──────────────────────────────────────────────────────────────
  //

  const { query } = parse(req.url, true)

  // Url
  let { url } = query
  if (!url.match(/https{0,1}:\/\/.*\..*/)) {
    url = 'http://' + url
  }

  // Alias
  let alias = query.alias
  // If no alias is defined then generate a random one
  if (alias === undefined) {
    do {
      // Generate random alias
      alias = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5)
    } while (await dbExists(alias)) // Re-Generate if alias is already in use
  } else {
    // For custom alias check if it is already in use
    if (await dbExists(alias)) {
      // If it is, throw error and return
      send(res, 409, { Error: { code: 2, message: `Alias already in use` } })
      return
    }

    if (!alias.match(/^[a-z0-9_-]*$/)) {
      // If custom alias includes not friendly characters, throw error and return
      send(res, 400, { Error: { code: 3, message: `Alias not only using lowercase characters, numbers, - and _` } })
      return
    }

    if (alias.match(/authentication|admin|dashboard|favicon\.ico/)) {
      send(res, 400, { Error: { code: 2, message: `Alias already in use` } })
      return
    }
  }

  //
  // ─── SAVING ─────────────────────────────────────────────────────────────────────
  //

  // Save new url and alias to db
  new UrlModel({ alias: alias, url: url }).save()
    .then(() => {
      // Respond to the user that it worked
      send(res, 200, { alias: alias, url: url })
    })
    .catch(err => {
      console.error(err)
      send(res, 500, { Error: { code: 6, message: `Server Error` } })
    })
}

async function dbExists(alias): Promise<boolean> {
  const x = await UrlModel.find({ alias: alias })
  return x.length !== 0
}

import { IncomingMessage, ServerResponse } from 'http'
import { send } from 'micro'
const { parse } = require("url")
const connectdb = require('./lib/db')

const UrlModel = require('./lib/urlModel')

module.exports = async (req: IncomingMessage, res: ServerResponse) => {

  //
  // ─── SETUP ──────────────────────────────────────────────────────────────────────
  //

  // Database Connection
  await connectdb()

  //
  // ─── INPUT PARSING ──────────────────────────────────────────────────────────────
  //

  const { alias } = parse(req.url, true).query

  //
  // ─── FIND ──────────────────────────────────────────────────────────────
  //

  UrlModel.findOne({ alias: alias }, (err, obj) => {
    if (err) console.error(err)
    if (!obj) {
      send(res, 404, { message: 'nothing found' })
    } else {

      obj = { alias: obj.alias, url: obj.url }

      send(res, 200, obj)
    }
  })
}
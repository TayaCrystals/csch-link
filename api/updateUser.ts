import { IncomingMessage, ServerResponse } from 'http'
import { send } from 'micro'
const { parse } = require("url")
const UserModel = require('./lib/userModel')
const connectdb = require('./lib/db')
const checkrole = require('./lib/role')

module.exports = async (req: IncomingMessage, res: ServerResponse) => {

  //
  // ─── SETUP ──────────────────────────────────────────────────────────────────────
  //

  // Require Role
  if (await checkrole(req) !== 'admin') {
    send(res, 401, { Error: { code: 5, message: `Not Autorized` } })
    return
  }

  // Database Connection
  await connectdb()

  //
  // ─── INPUT PARSING ──────────────────────────────────────────────────────────────
  //

  const { query } = parse(req.url, true)

  const { username, role } = query

  let mongoobj = await UserModel.findOne({ username: username })

  //
  // ─── SAVING ─────────────────────────────────────────────────────────────────────
  //

  mongoobj.role = role

  mongoobj.save()
    .then(() => {
      // Respond to the user that it worked
      send(res, 200, { username: username, role: role })
    })
    .catch(err => {
      console.error(err)
      send(res, 500, { Error: { code: 6, message: `Server Error` } })
    })
}